from __future__ import annotations

from homeassistant.core import HomeAssistant
from homeassistant.helpers.typing import ConfigType

from PyDMXControl.controllers import uDMXController

DOMAIN = 'dmx_lights'


def setup(hass: HomeAssistant, config: ConfigType) -> bool:
    """Your controller/hub specific code."""
    # Data that you want to share with your platforms
    hass.data[DOMAIN] = {
        "controller": uDMXController(),
    }

    hass.helpers.discovery.load_platform('light', DOMAIN, {}, config)

    return True
