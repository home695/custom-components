from __future__ import annotations

import logging

import voluptuous as vol

# Import the device class from the component that you want to support
import homeassistant.helpers.config_validation as cv
from homeassistant.components.light import (
  COLOR_MODE_BRIGHTNESS, ATTR_BRIGHTNESS, PLATFORM_SCHEMA, LightEntity)

from homeassistant.const import CONF_DEVICES, CONF_NAME, CONF_HOST
from homeassistant.core import HomeAssistant
from homeassistant.helpers.entity_platform import AddEntitiesCallback
from homeassistant.helpers.typing import ConfigType, DiscoveryInfoType

# 3rd party imports
#from PyDMXControl.controllers import uDMXController
from PyDMXControl.profiles.Generic import Dimmer

_LOGGER = logging.getLogger(__name__)
DOMAIN = 'dmx_lights'
CONF_CHANNEL = 'channel'

DEVICE_SCHEMA = vol.Schema({
    vol.Required(CONF_NAME): cv.string,
    vol.Required(CONF_CHANNEL): cv.positive_int,
})

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Optional(CONF_DEVICES, default={}): {cv.string: DEVICE_SCHEMA}
})

_LOGGER = logging.getLogger(__name__)

def setup_platform(
    hass: HomeAssistant,
    config: ConfigType,
    add_entities: AddEntitiesCallback,
    discovery_info: DiscoveryInfoType | None = None
) -> None:
    _LOGGER.info("DMX lights config started!")
    controller = hass.data[DOMAIN].get('controller')

    lights = []

    for device_id, device_config in config.get(CONF_DEVICES, {}).items():
        name = device_config[CONF_NAME]
        channel = device_config[CONF_CHANNEL]
        _LOGGER.info(name)
        lights.append(DMXLight(controller, name, channel))

    add_entities(lights)
    _LOGGER.info("DMX lights complete!")


class DMXLight(LightEntity):

    SMOOTH_MS = 500

    def __init__(self, controller, name, channel) -> None:
        self._name = name
        self._channel = channel
        self._controller = controller
        self._fixture = self._controller.add_fixture(Dimmer, name=name, start_channel=channel).dim(0)
        self._brightness = 0
        self._state = False


    @property
    def unique_id(self) -> str | None:
        """unique_id of the entity."""
        return f"dmx_{self._channel:02d}"

    @property
    def supported_color_modes(self) -> set[str] | None:
        """Flag supported features."""
        return [COLOR_MODE_BRIGHTNESS]

#    @property
#    def should_poll(self):
#        return False

    @property
    def name(self) -> str:
        return self._name

    @property
    def brightness(self):
        return self._brightness

    @property
    def is_on(self) -> bool | None:
        return self._state

    def turn_on(self, **kwargs) -> None:
        self._state = True

        self._brightness = kwargs.get(ATTR_BRIGHTNESS, 255)
        self._fixture.dim(self._brightness, self.SMOOTH_MS)
        #self._fixture.dim(255, 500)

    def turn_off(self, **kwargs) -> None:
        self._state = False

        self._brightness = 0
        self._fixture.dim(self._brightness, self.SMOOTH_MS)

    def update(self) -> None:
        pass
        #self._state = self.is_on()    
